<?php
    $servername = "localhost";
    $database = "teachers";
    $username = "root";
    $password = "";

    // Create connection
    $conn = mysqli_connect($servername, $username, $password, $database);
    mysqli_set_charset($conn, "utf8");
    $sql = "SELECT id, name, avatar, description, specialized, degree, updated, created FROM teachers";
    $result = $conn->query($sql);

    function getQuerySelectAll($conn){
        $query = mysqli_query($conn, "SELECT * FROM `teachers`");
        return $query;
    }

    function getQueryWithoutSubject($conn, $keysubject, $keyword){
        $query = mysqli_query($conn, "SELECT teachers.* FROM `teachers` INNER JOIN  `degree`   on teachers.degree = degree.id
                                                                        WHERE teachers.name LIKE '%$keyword%'  
                                                                        or teachers.description LIKE '%$keyword%' 
                                                                        or teachers.degree LIKE '%$keyword%' 
                                                                        or degree.name LIKE '%$keyword%' ");
        return $query;
    }

    function getQueryWithSubject($conn, $keysubject, $keyword){
        $query = mysqli_query($conn, "SELECT teachers.* FROM `teachers` INNER JOIN `degree` on teachers.degree = degree.id
                                                                        WHERE teachers.specialized = '$keysubject'  
                                                                        and  (teachers.name LIKE '%$keyword%'  
                                                                        or teachers.description LIKE '%$keyword%' 
                                                                        or teachers.degree LIKE '%$keyword%'
                                                                        or degree.name LIKE '%$keyword%')");
        return $query;
    }

?>