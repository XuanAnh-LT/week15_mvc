<!DOCTYPE html>
<html lang='en'>
<head>
    <meta charset='UTF-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <link rel='stylesheet' href='teacher_search.css'>
    <title>Teacher_search</title>
</head>

<?php
    include_once(dirname(__FILE__).'/../model/teacher.php');
    include_once(dirname(__FILE__).'/../controller/teacher_search.php');
?>
    <style>
        <?php include_once(dirname(__FILE__).'/../teacher_search.css'); ?>
    </style>

<body>

<form action='' method='GET'>
    <div class='container_margin'>
        <table class='search_box' cellspacing='15'>
            <tr>
                <td class='search_box_row'>Khoa:</td>
                <td><select class='box' id='search_box_subject' name='search_subject' value = ''>
                <?php
                    createSearchSubject($subject);
                ?>
                </td> 
            </tr>
    
            <tr>
                <td class='search_box_row'>Từ khóa:</td>
                <td><input type='text' class='box' id='search_box_keyword' name='search_keyword' value = <?php echo isset($_SESSION['search_keyword']) ? $_SESSION['search_keyword'] : '' ?> ></td>
            </tr>
        </table>

        <br>

        <div class='align_center'>
            <input class='button_search' type='submit' name='search' value='Tìm kiếm'></input>
        </div>

        <br><br><br>
        
        <?php 
            if(!isset($_GET['search'])){
                $query = getQuerySelectAll($conn);
            } else{
                echo "
                    <script>
                        document.getElementById('count_record').remove();
                    </script>
                ";
                if(empty($_GET['search_subject'])){
                    $query = getQueryWithoutSubject($conn, $keysubject, $keyword);
                } else{
                   $query = getQueryWithSubject($conn, $keysubject, $keyword);
                }
                
            }
            $rowcount = mysqli_num_rows( $query );
            echo "<label id='count_record' style='text-align: left;'>Số giáo viên tìm thấy: ".$rowcount." </label>";

        ?>

        <br><br>

        <table  style='width: 100%'>
            <thead>
                <td style='width: 5%'>No</td>
                <td style='width: 20%'>Tên giáo viên</td>
                <td style='width: 20%'>Khoa</td>
                <td style='width: 32%'>Mô tả chi tiết</td>
                <td style='width: 20%'>Action</td>
            </thead>

            <tbody>
            <?php
                if ($result->num_rows > 0) {

                    if (isset($_GET['search'])) {
                        if(empty($_GET['search_subject'])){
                            $query = getQueryWithoutSubject($conn, $keysubject, $keyword);
                        } else{
                            $query = getQueryWithSubject($conn, $keysubject, $keyword);
                        }
                    $rowcount = mysqli_num_rows( $query );
                    while($row = mysqli_fetch_array($query)){ 
                            printResult($row);    
                        }
                    }
                    // Load dữ liệu lên website
                    else{
                        while($row = $result->fetch_assoc()) {
                            printResult($row);
                            }
                        }
                    } 
                    $conn->close();
                    ?>
            </tbody>
        </table>
    
    </div>
</form>
    
<script>
            function ClearFields() {
                document.getElementById("keysubject").value = "";
                document.getElementById("keyword").value = "";
                sessionStorage.clear();
                }
</script>

<?php 
    session_destroy();
?>

</body>
</html>